# fakevim

This is a major *HACK* that brings a basic VIM-style modal editor to
GtkSourceView.

So that it can be used inside of Builder (my current project) as well as other
Gtk/GNOME places, I tried to make it not subclass GbSourceView. Instead it
just hooks to events to get everything it needs.

Currently, the word selection algorithm needs work. There are bugs filed in
upstream Gtk that should land in git master over the next few weeks. Worst case
in 3.15.x.

This doesn't have command support. That is left as an exercise for you.

```sh
make
./fakevim
```

Cheers!

Christian

