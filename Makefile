all: fakevim

FILES = gb-source-vim.c gb-source-vim.h main.c
PKGS = gtksourceview-3.0

fakevim: $(FILES)
	$(CC) -o $@.tmp -Wall $(FILES) $(shell pkg-config --cflags --libs $(PKGS)) -DGB_SOURCE_VIM_EXTERNAL
	mv $@.tmp $@

clean:
	rm -f fakevim
