#include <gtk/gtk.h>
/* main.c
 *
 * Copyright (C) 2014 Christian Hergert <christian@hergert.me>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtksourceview/gtksource.h>

#include "gb-source-vim.h"

static GtkRevealer *revealer;
static GtkEntry *entry;
static GbSourceVim *vim;
static GtkSourceView *source_view;

static void
command_visibility_toggled (GbSourceVim *vim,
                            gboolean     visible,
                            gpointer     user_data)
{
  gtk_revealer_set_reveal_child (revealer, visible);
  gtk_widget_grab_focus (visible? GTK_WIDGET (entry): GTK_WIDGET (source_view));
}

static void
entry_activate (GtkEntry *entry,
                gpointer  user_data)
{
  gb_source_vim_execute_command (vim, gtk_entry_get_text (entry));
}

static gboolean
entry_key_press (GtkEntry    *entry,
                 GdkEventKey *event,
                 gpointer     user_data)
{
  if (event->keyval == GDK_KEY_Escape)
    {
      gb_source_vim_set_mode (vim, GB_SOURCE_VIM_NORMAL);
      return TRUE;
    }

  return FALSE;
}

gint
main (gint   argc,
      gchar *argv[])
{
  GtkWindow *window;
  GtkScrolledWindow *scroller;
  GtkSourceBuffer *buffer;
  PangoFontDescription *font;
  GtkSourceStyleScheme *scheme;
  GtkSourceLanguage *lang;
  GtkOverlay *overlay;
  GtkHeaderBar *header_bar;
  GtkSwitch *vim_switch;

  gtk_init (&argc, &argv);

  window = g_object_new (GTK_TYPE_WINDOW,
                         "title", "FakeVim",
                         "default-width", 800,
                         "default-height", 600,
                         NULL);
  g_signal_connect (window, "delete-event", gtk_main_quit, NULL);

  header_bar = g_object_new (GTK_TYPE_HEADER_BAR,
                             "title", "FakeVIM",
                             "visible", TRUE,
                             "show-close-button", TRUE,
                             NULL);
  gtk_window_set_titlebar (window, GTK_WIDGET (header_bar));

  vim_switch = g_object_new (GTK_TYPE_SWITCH,
                             "active", TRUE,
                             "tooltip-text", "Enable VIM compatability mode.",
                             "visible", TRUE,
                             NULL);
  gtk_header_bar_pack_start (header_bar, GTK_WIDGET (vim_switch));

  overlay = g_object_new (GTK_TYPE_OVERLAY,
                          "visible", TRUE,
                          NULL);
  gtk_container_add (GTK_CONTAINER (window), GTK_WIDGET (overlay));

  revealer = g_object_new (GTK_TYPE_REVEALER,
                           "visible", TRUE,
                           "valign", GTK_ALIGN_END,
                           "hexpand", TRUE,
                           "reveal-child", FALSE,
                           "transition-type", GTK_REVEALER_TRANSITION_TYPE_SLIDE_UP,
                           NULL);
  gtk_overlay_add_overlay (overlay, GTK_WIDGET (revealer));

  entry = g_object_new (GTK_TYPE_ENTRY,
                        "has-frame", FALSE,
                        "visible", TRUE,
                        NULL);
  g_signal_connect (entry, "activate", G_CALLBACK (entry_activate), NULL);
  g_signal_connect (entry, "key-press-event", G_CALLBACK (entry_key_press), NULL);
  gtk_container_add (GTK_CONTAINER (revealer), GTK_WIDGET (entry));

  scroller = g_object_new (GTK_TYPE_SCROLLED_WINDOW,
                           "visible", TRUE,
                           NULL);
  gtk_container_add (GTK_CONTAINER (overlay), GTK_WIDGET (scroller));

  lang = gtk_source_language_manager_get_language (
    gtk_source_language_manager_get_default (), "c");
  scheme = gtk_source_style_scheme_manager_get_scheme (
    gtk_source_style_scheme_manager_get_default (), "tango");

  buffer = g_object_new (GTK_SOURCE_TYPE_BUFFER,
                         "language", lang,
                         "style-scheme", scheme,
                         NULL);

  source_view = g_object_new (GTK_SOURCE_TYPE_VIEW,
                              "auto-indent", TRUE,
                              "buffer", buffer,
                              "show-line-numbers", TRUE,
                              "show-right-margin", TRUE,
                              "right-margin-position", 80,
                              "indent-width", 2,
                              "tab-width", 2,
                              "insert-spaces-instead-of-tabs", TRUE,
                              "visible", TRUE,
                              NULL);
  gtk_container_add (GTK_CONTAINER (scroller), GTK_WIDGET (source_view));

  font = pango_font_description_from_string ("Monospace 10");
  gtk_widget_override_font (GTK_WIDGET (source_view), font);
  pango_font_description_free (font);

  vim = g_object_new (GB_TYPE_SOURCE_VIM,
                      "text-view", source_view,
                      "enabled", TRUE,
                      NULL);
  g_signal_connect (vim,
                    "command-visibility-toggled",
                    G_CALLBACK (command_visibility_toggled),
                    NULL);
  g_object_bind_property (vim_switch, "active", vim, "enabled",
                          G_BINDING_SYNC_CREATE);

  /*
   * XXX: no phrase visualization, no mode status.
   *
   * GbSourceVim::notify::phrase:
   * GbSourceVim::notify::mode:
   */

  gtk_window_present (window);
  gtk_main ();

  return 0;
}
